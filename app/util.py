from app.models import db, Messages, Settings
import datetime
import requests
from suds.client import Client
from suds.transport.http import HttpAuthenticated
from suds.transport import Reply, TransportError
from threading import Thread
from app import app
import logging

logging.basicConfig(level=logging.INFO)
logging.getLogger('suds.client').setLevel(logging.DEBUG)
logging.getLogger('suds.transport').setLevel(logging.DEBUG)

# SMS_GATEWAY_URL = 'http://184.173.247.178:13013/cgi-bin/sendsms?username= &password= &to=&from=&text='
SMS_GATEWAY_URL = 'http://184.173.247.178:13013/cgi-bin/sendsms'
SMS_GATEWAY_USERNAME = 'odun1234'
SMS_GATEWAY_PASSWORD = 'odun@1234'
SHORTCODE = '33070'


class RequestsTransport(HttpAuthenticated):
    def __init__(self, **kwargs):
        self.cert = kwargs.pop('cert', None)
        # super won't work because not using new style class
        HttpAuthenticated.__init__(self, **kwargs)

    def send(self, request):
        self.addcredentials(request)
        resp = requests.post(request.url, data=request.message,
                             headers=request.headers, cert=self.cert)
        result = Reply(resp.status_code, resp.headers, resp.content)
        return result


def async(f):
    '''
    Decorator to process asynchronous calls
    '''
    def wrapper(*args, **kwargs):
        thr = Thread(target=f, args=args, kwargs=kwargs)
        thr.start()
    return wrapper


def get_message(slug):
    return Messages.query.filter(Messages.slug == slug.lower()).one_or_none()


def send_sms(msisdn, text_message):
    payload = {'username': SMS_GATEWAY_USERNAME, 'password': SMS_GATEWAY_PASSWORD, 'to': msisdn,
               'from': SHORTCODE, 'text': text_message}
    requests.get(SMS_GATEWAY_URL, params=payload)


def generate_ticket():
    token = "WC3" + datetime.date.today().strftime('%Y%m%d')

    # get current token
    current = Settings.query.get(1)

    if current:
        next_int = current.max_ticket + 1
    else:
        next_int = '1'
        current = Settings()

    token = token + str(next_int).zfill(7)
    current.max_ticket = next_int
    db.session.add(current)
    db.session.commit()

    return token


@async
def transaction_notification(ticket, network, msisdn, drawing_date, transaction_timestamp, amount):
    '''
    Notify NLRC of valid entry
    :param ticket: String (alphanumeric)
    :param network: String (MTN/GLO/AIRT/ETI)
    :param msisdn: String
    :param drawing_date: String (DateTime: '%Y%m%d%H%M' eg 201812231754)
    :param transaction_timestamp: String (DateTime: '%Y%m%d%H%M%S' eg 20181223175450)
    :param amount: String
    :return: response dictionary
    '''

    # url = 'http://arsblue.net:9448/AsoftNLGS/endpoints/SaveTransactionService.wsdl'
    url = 'http://arsblue.net:9448/AsoftNLGS/endpoints/SaveTransactionPropService.wsdl'
    # url = 'http://arsblue.net:9448/AsoftNLGS/endpoints/SaveTransactionPropService.wsdl'
    # url = 'https://app.arsblue.rs:9445/AsoftNLGS/endpoints/SaveTransactionService.wsdl'

    # client = Client(url, transport=HTTPSClientCertTransport(
    # '/media/Storage/Projects/python-apps/lottery/app/certificates/arsblue_NLGS_client.pem',
    #    '/media/Storage/Projects/python-apps/lottery/app/certificates/arsblue_NLGS_client.pem'))
    # cert = '/media/Storage/Projects/python-apps/lottery/app/certificates/arsblue_NLGS_client.pem'
    cert = '/var/www/lottery/lottery/app/certificates/arsblue_NLGS_client.pem'

    headers = {"Content-Type": "text/xml;charset=UTF-8", "SOAPAction": ""}
    t = RequestsTransport(cert=cert)
    client = Client(url, headers=headers, transport=t)

    print("Inside transaction notification")
    print "Calling API"
    retval = client.service.TransactionProp(ticketReference=ticket, permitHolderId=37,
                                            drawingDateTime=drawing_date, gameType=280, transactionType=1,
                                            source=1, providerIdentifier=network,
                                            transactionTimestamp=transaction_timestamp, transactionAmount=amount,
                                            sourceIdentifier=msisdn)
    print retval