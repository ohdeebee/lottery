from flask import Flask
from app.config import Config
from app.models import db

app = Flask(__name__)
app.config.from_object(Config)
db.init_app(app)

# for apis
from app.api.api import api
app.register_blueprint(api, url_prefix='/api')