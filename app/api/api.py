from flask import Blueprint, request, jsonify
from app.models import RequestLog, Categories, Entries
from app.util import send_sms, get_message, generate_ticket, transaction_notification
from app import app, db

api = Blueprint('api', __name__)


@api.route('/', methods=['GET'])
@api.route('/index', methods=['GET'])
def index():
    return jsonify({'status': 201, 'message': 'Received'})


@api.route('/v1/transaction', methods=['GET'])
def transaction():
    # get parameters
    msisdn = request.args.get('sender')
    shortcode = request.args.get('shortcode')
    message = request.args.get('message')
    message_id = request.args.get('messageid')
    network = request.args.get('operator')
    network_timestamp = request.args.get('timestamp')

    # log request
    new_request = RequestLog(msisdn, message, message_id, network_timestamp, shortcode, network)
    db.session.add(new_request)
    db.session.commit()

    print 'got 1'
    # check if keyword is help
    if message.lower() == 'help':
        print 'got 2'
        send_sms(msisdn, get_message('help').format_message())
    else:
        # check if keyword matches any of our keywords
        print message
        print len(message)

        cat = Categories.query.filter(Categories.keywords == message.lower()).one_or_none()

        if not cat:
            print 'got here 3'
            # if it doesnt, send invalid message
            send_sms(msisdn, "Invalid keyword. Reply HELP to receive a list of available games.")
        else:
            print 'got hhere'
            # if it does, log into entries table
            entry = Entries()
            entry.msisdn = msisdn
            entry.network = network
            entry.amount_paid = cat.amount_to_play
            entry.category_id = cat.id
            entry.ticket = generate_ticket()

            # save
            db.session.add(entry)
            db.session.commit()

            # call api
            transaction_notification(entry.ticket, network, msisdn, network_timestamp,
                                     entry.date_created.strftime('%Y%m%d%H%M%S'), entry.amount_paid)

            # send successful message
            # send_sms(msisdn, get_message('success').format_message(amount=cat.amount_to_play))

    return jsonify({'status': 201, 'message': 'Success'}), 201