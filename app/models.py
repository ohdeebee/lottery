from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import Column, Integer, String, DateTime, func, Text, ForeignKey
from slugify import slugify

db = SQLAlchemy()


class Categories(db.Model):
    __tablename__ = 'categories'

    id = Column(Integer, primary_key=True)
    name = Column(String(100))
    slug = Column(String(100))
    amount_to_play = Column(String(100))
    keywords = Column(Text)
    grand_prize = Column(String(100))
    consolation_prize = Column(String(100))
    date_created = Column(DateTime, default=func.now())

    def __init__(self, name, amount_to_play, keywords, grand_prize, consolation):
        self.name = name
        self.amount_to_play = amount_to_play
        self.keywords = keywords
        self.grand_prize = grand_prize
        self.consolation_prize = consolation
        self.create_slug()

    def create_slug(self):
        self.slug = slugify(self.name)


class RequestLog(db.Model):
    __tablename__ = 'request_log'

    id = Column(Integer, primary_key=True)
    msisdn = Column(String(30))
    message = Column(Text)
    message_id = Column(String(50))
    network_timestamp = Column(String(100))
    shortcode = Column(String(20))
    network = Column(String(20))
    date_created = Column(DateTime, default=func.now())

    def __init__(self, msisdn, message, message_id, network_timestamp, shortcode, network):
        self.msisdn = msisdn
        self.message = message
        self.message_id = message_id
        self.network_timestamp = network_timestamp
        self.shortcode = shortcode
        self.network = network


class Messages(db.Model):
    __tablename__ = 'messages'

    id = Column(Integer, primary_key=True)
    name = Column(String(50))
    slug = Column(String(50))
    message = Column(Text)
    date_created = Column(DateTime, default=func.now())

    def create_slug(self):
        self.slug = slugify(self.name)

    def format_message(self, **kwargs):
        message = self.message
        if kwargs is not None:
            for key, value in kwargs.iteritems():
                message = message.replace('#' + str(key).upper(), str(value))
        return message


class Entries(db.Model):
    __tablename__ = 'entries'

    id = Column(Integer, primary_key=True)
    ticket = Column(String(50), unique=True)
    msisdn = Column(String(30))
    network = Column(String(50))
    amount_paid = Column(String(30))
    category_id = Column(Integer, ForeignKey('categories.id'))
    status = Column(Integer, default=0)
    date_created = Column(DateTime, default=func.now())


class Settings(db.Model):
    __tablename__ = 'settings'

    id = Column(Integer, primary_key=True)
    max_ticket = Column(Integer)